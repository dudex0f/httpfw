#!/usr/bin/python2

# -*- coding: utf-8 -*-

import logging
from time import time
from os import path, sep

try:
    from requests import Session, exceptions
except ImportError:
    logging.critical("Can't load module requests. Not installed?")
    raise

try:
    from bs4 import BeautifulSoup
except ImportError:
    logging.critical("Can't load module BeautifulSoup. Not installed?")
    raise

import logging
from inspect import getframeinfo, stack
from urlparse import urlsplit
from traceback import format_exc


class Httpfw(Session):
    def __init__(self):
        Session.__init__(self)
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:26.0) Gecko/20100666 Firefox/36.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8,pl;q=0.6',
            'DNT': '1',
            'Accept-Encoding': "gzip,deflate,sdch"
        }

    def try_http_request(self, request_url, request_method='get', request_data=''):
        timeout = 30
        response = None
        logging.debug('%s method to url = %s, with data = %s' % (request_method, request_url, request_data))
        if request_method == 'get':
            try:
                response = self.get(request_url, timeout=timeout)
                logging.debug('response code: %s, %i' % (response.reason, response.status_code))
                if response.status_code == 500:
                    logging.error('%s to %s returned response code: %s, %i' % (
                        request_method, request_url, response.reason, response.status_code))
                    raise IOError
            except exceptions.ConnectionError:
                logging.error('ERROR: Connection to: %s failed ' % request_url)
                raise
        if request_method == 'post':
            try:
                response = self.post(request_url, data=request_data, timeout=timeout)
                logging.debug('response code: %s, %i' % (response.reason, response.status_code))
                if response.status_code == 500:
                    logging.error('%s to %s returned response code: %s, %i' % (
                        request_method, request_url, response.reason, response.status_code))
                    raise IOError
            except exceptions.ConnectionError:
                logging.error('ERROR: Connection to: %s failed ' % request_url)
                raise
        return response

    def get_soup_from_url(self, url, request_method='get', request_data=''):

        soup = None
        try:
            response = self.try_http_request(url, request_method=request_method, request_data=request_data)
            encoding = response.encoding
            logging.debug('found encoding %s' % encoding)

            soup = BeautifulSoup(response.content, from_encoding=encoding)
        except:
            logging.error('Failed to get soup from url %s. Got exception: %s' % (url, format_exc()))

        return soup

    def get_form_data_from_html_code(self, html_code, form_filter=None, raise_exception_if_form_not_found=True):
        """Extract all <input> tags with value attrs from <form> tag found in html_code.
        
        Keyword arguments:
        html_code -- hmtl code to scan
        form_filter -- dictionary with other attributes of the form to pass to beautiful soup (default empty dir)
        
        Returns form_data dictionary {'action': '', 'method': '', 'inputs': {'name': 'value''}}
        or raise Exception if form not found
        
        """
        if not form_filter:
            form_filter = {}
        logging.debug('Start: passed parameters: form_filter = %s' % (str(form_filter)))
        return_dict = {'action': '', 'id': '', 'method': 'get', 'inputs': dict()}

        try:
            soup = BeautifulSoup(html_code)
            logging.debug('Successfully parsed html code')
        except:
            logging.error('Failed to parse given html code')
            dump_to_file(html_code)
            raise

        # logging.debug('Will run find() with the following parameters: %s' % str(find_attrs))

        form = soup.find(name='form', attrs=form_filter)
        if form:
            logging.debug('Found form in soup')
        else:
            logging.error('Form NOT in soup')
            if raise_exception_if_form_not_found:
                dump_to_file(html_code)
                raise Exception('Form NOT found')
            else:
                return None

        try:
            return_dict['action'] = form['action']
            logging.debug('Found form action: %s' % return_dict['action'].encode('utf-8'))
        except:
            logging.error('action attribute not found in the form')
            raise

        try:
            return_dict['method'] = form['method'].lower()
            logging.debug('Found form method: %s' % return_dict['method'].encode('utf-8'))
        except:
            logging.warning(
                'method attribute not found in the form. Default to: %s' % return_dict['method'].encode('utf-8'))
            pass

        try:
            return_dict['id'] = form['id']
            logging.debug('Found form id: %s' % return_dict['id'].encode('utf-8'))
        except:
            logging.warning('id attribute not found in the form')
            pass

        form_data = {}
        for input_tag in form.findAll('input'):
            try:
                param = input_tag['name'].encode('utf-8')
                try:
                    val = input_tag['value'].encode('utf-8')
                except:
                    val = ''
            except:
                pass
            logging.debug('Found input field: %s = %s' % (param, val))
            form_data[param] = val
        return_dict['inputs'] = form_data
        logging.debug('Finished: return object: %s' % str(return_dict))
        return return_dict

    def get_form_data_from_url(self, url, form_filter=None, raise_exception_if_form_not_found=True):
        """Extract all <input> tags with value attrs from <form> tag found in html_code from url.
        
        Keyword arguments:
        url -- url to get html code
        form_filter -- dictionary with other attributes of the form to pass to beautiful soup (default empty dir)
        
        Returns form_data dictionary {'action': '', 'method': '', 'inputs': {'name': 'value''}, 'response': response}
        or raise Exception if form not found
        
        """
        if not form_filter:
            form_filter = {}
        logging.debug('Start: passed parameters: url: %s, form_filter = %s' % (url, str(form_filter)))
        return_dict = {'action': '', 'method': 'get', 'inputs': dict()}

        response = self.try_http_request(url)

        form_dict = self.get_form_data_from_html_code(response.content,
                                                      form_filter,
                                                      raise_exception_if_form_not_found=raise_exception_if_form_not_found)
        if not form_dict:
            return None

        form_dict['response'] = response
        if form_dict['action'][0] == '/':
            logging.warning('Form action %s not an URL. Adding schema and netloc from form URL %s'
                            % (str(form_dict['action']).encode('utf-8'), response.url))
            form_dict['action'] = get_schema_and_netloc_from_url(response.url) + form_dict['action']

        logging.debug('Finished: return object: %s' % str(form_dict))
        return form_dict


def dump_to_file(webcontent, fname=''):
    try:
        calling_filename = getframeinfo(stack()[-1][0])[0]
        logging.debug('calling filename: %s' % calling_filename)
    except:
        logging.error('Failed to determine call stack')
        calling_filename = __file__

    output_dir = path.dirname(path.realpath(calling_filename))

    if fname == '':
        fname = str(int(time())) + '.html'

    fname = output_dir + sep + fname

    logging.info('content dumped to %s' % fname)
    with open(fname, 'w') as dumpfile:
        dumpfile.write(webcontent)


def get_form_data_by_id(webcontent, form_id=''):
    htmldoc = BeautifulSoup(webcontent)

    if form_id:
        form = htmldoc.find('form', id=form_id)
    else:
        form = htmldoc.find('form')
    # form = form[0]
    # print form['method']
    if not form:
        logging.error('No form with id=%s found in the webcontent\n%s' % (form_id, webcontent))
        return None
    logging.debug('Found form: %s' % form.prettify())
    try:
        form_action = form['action']
        logging.debug('Found form action: %s' % form_action)
    except:
        logging.error('Failed to find form action')
        form_action = None

    form_data = {}
    for input_tag in form.findAll('input'):
        param = val = ""
        try:
            param = input_tag['name'].encode('utf-8')
            try:
                val = input_tag['value'].encode('utf-8')
            except:
                val = ''
        except:
            pass
        form_data[param] = val
    logging.debug('Form data found for form_id = %s.\nform_data =  %s' % (form_id, form_data))
    return form_data


def get_schema_and_netloc_from_url(url):
    return str(urlsplit(url)[0] + '://' + urlsplit(url)[1])


#########################################################

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s %(levelname)s: [%(module)s.%(funcName)s] %(message)s', level='DEBUG')
    session = Httpfw()
    session.get_form_data_from_url('https://ssl.allegro.pl/fnd/authentication/', form_filter={'id': 'auth-form'})
    # session.get_form_data_from_url('https://ssl.allegro.pl/fnd/authentication/')
