#!/usr/bin/python2
#-*- coding: utf-8 -*-

from time import time 
from os import path, sep
import requests
from bs4 import BeautifulSoup
import logging


##############         session settings
def initHttpSession():
    session = requests.Session()
    session.headers = { 
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:38.0) Gecko/20100666 Firefox/38.0', 
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8,pl;q=0.6', 
            'DNT':'1',
            'Accept-Encoding': "gzip,deflate,sdch" 
    }
    return session

def dumpToFile(webcontent, fname=''):
     if fname == '':
         fname = path.dirname( path.realpath(__file__)) + sep + str(int(time())) + '.html'
     print 'content dumped to', fname, 'file'
     with open(fname, 'w') as dumpfile:
          dumpfile.write(webcontent)

def getFormDataById(webcontent, formId=''):
    htmldoc = BeautifulSoup(webcontent)

    if formId:
        form = htmldoc.find('form', id=formId)
    else:
        form = htmldoc.find('form')
    # form = form[0]
    # print form['method']
    if not form: return None

    form_data = {}
    for input in form.findAll('input'):
        try:
            param = input['name'].encode('utf-8')
            try:
                val = input['value'].encode('utf-8')
            except:
                val = ''
        except:
            pass
        form_data[param] = val

    # print form_data
    return form_data

